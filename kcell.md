# Оглавление

1. [Бизнес требования](#бизнес-требования)
2. [Фигма](#Фигма)
3. [Ресурсная модель](#Ресурная-модель)
4. [ER диаграмма](#ER-диарамма)
5. [Описание эндпойнтов](#Описание-эндпойнтов)
6. [Sequence диаграмма](#Sequence-диаграмма)
7. [Часть два](#Часть-два)
8. [Часть три](#Часть-три)

## Бизнес требования
Ссылка на файл: [Тестовое задание Netflix](Тестовое задание Netflix 1.docx)

## Фигма
Ссылка на мокап: [мокап](https://www.figma.com/design/qtOibQENfdDOIrhok8wEmz/Untitled?node-id=0-1)


## Ресурная модель

```java
public class Users {
    private Long id;
    private String phoneNumber;
    private String email;
    private double balance;
}

public class Subscriptions {
    private Long id;
    private String name;
    private String description;
    private String type; //netflix, а мб что то еще в будущем
    private Price price; //private double price -  изменен с учетом добавления разной тарификации по сроку подключения
}

public class Price {
    private double daily;
    private double weekly;
    private double monthly;
}

public class UserSubscription {
    private Long id;
    private Long userId;
    private Long subscriptionId;
    private String status;
    private String activationDate;
    private String nextBillingDate; 
    private String period;
}

public class Transactions{
    private Long id;
    private Long userSubscriptionId;
    private String transactionId;
    private String paymentToken; //если платежка будет обработано извне через третью сторону
    private String status; //возможно будет сертификация
}

```

## ER диаграмма


```mermaid
erDiagram
    USERS {
        Long id
        String phoneNumber
        String email
        double balance
    }
    
    SUBSCRIPTIONS {
        Long id
        String name
        String description
        String type
        Long priceId
    }
    
    PRICE {
        Long id
        double daily
        double weekly
        double monthly
    }
    
    USER_SUBSCRIPTION {
        Long id
        Long userId
        Long subscriptionId
        String status
        String activationDate
        String nextBillingDate
        String period
    }
    
    TRANSACTIONS {
        Long id
        Long userSubscriptionId
        String transactionId
        String paymentToken
        String status
    }
    
    USERS ||--o{ USER_SUBSCRIPTION: "has"
    SUBSCRIPTIONS ||--o{ USER_SUBSCRIPTION: "contains"
    PRICE ||--o| SUBSCRIPTIONS: "has"
    USER_SUBSCRIPTION ||--o{ TRANSACTIONS: "generates"


```

### Таблицы и их описания (без учета системных полей)

1. **USERS**
   - Хранит данные пользователей.
   - Поля:
     - `Long id` - Идентификатор пользователя
     - `String phoneNumber` - Номер телефона
     - `String email` - Электронная почта
     - `double balance` - Баланс

2. **SUBSCRIPTIONS**
   - Хранит данные подписок, которые могут включать различные типы сервисов.
   - Поля:
     - `Long id` - Идентификатор подписки
     - `String name` - Название подписки
     - `String description` - Описание
     - `String type` - Тип  (например, Netflix)
     - `Long priceId` - Идентификатор стоимости подписки

3. **PRICE**
   - Таблица будет хранить данные о стоимости подписок в зависимости от периода
   - Поля:
     - `Long id` - Идентификатор стоимости
     - `double daily` - Стоимость подписки за день
     - `double weekly` - Стоимость за неделю
     - `double monthly` - Стоимость подписки за месяц

4. **USER_SUBSCRIPTION**
   - Связывает пользователей с подписками, хранит статус подписки и даты для биллинга.
   - Поля:
     - `Long id` - Идентификатор пользовательской подписки
     - `Long userId` - Идентификатор пользователя
     - `Long subscriptionId` - Идентификатор подписки
     - `String status` - Статус подписки (активна, приостановлена и другие возможные согласно бизнес требованиям)
     - `String activationDate` - Дата активации подписки
     - `String nextBillingDate` - Дата следующего списания средств
     - `String period` - Периодичность списания АП
     

5. **TRANSACTIONS**
   - Хранит данные о транзакциях, связанных с подписками пользователей.
   - Поля:
     - `Long id` - Идентификатор транзакции
     - `Long userSubscriptionId` - Идентификатор пользовательской подписки
     - `String transactionId` - Идентификатор транзакции
     - `String paymentToken` - Токен оплаты
     - `String status` - Статус транзакции (если будет сертификация)



## Описание эндпойнтов

### Получение списка всех доступных подписок

```http request
GET /subscriptions
```

**Response**

```json
[
  {
    "id": 1,
    "name": "Netflix",
    "description": "Streaming service",
    "type": "netflix",
    "priceId": 1
  },
  {
    "id": 2,
    "name": "SomeService",
    "description": "Streaming service",
    "type": "someservice",
    "priceId": 2
  }
]
```

### Подписка на определенный сервис (с указанием периода: день/неделя/месяц)

```http request
POST /users/{userId}/subscriptions/{subscriptionId}
```

**Request**

```json
{
  "userId":1,
  "subscriptionId": 1,
  "period": "monthly", //"daily", "weekly", "monthly"
  "activationDate": "2023-01-01"
}
```

**Response**

```json
{
  "id": 1,
  "userId": 1,
  "subscriptionId": 1,
  "status": "active", //active, deactive
  "activationDate": "2023-01-01",
  "nextBillingDate": "2023-02-01",
  "period": "monthly"
}

```

### Отмена подписки на определенный сервис

```http request
PATCH /users/{userId}/subscriptions/{subscriptionId}
```

**Request**

```json
{
  "userId":1, 
  "subscriptionId": 1,
  "status": "deactive"
}
```

**Response**

```json
{
  "message": "Subscription cancelled successfully",
  "subscriptionId": 1,
  "userId": 1,
  "status": "deactive"
}

```

## Sequence диаграмма

```mermaid
sequenceDiagram
    participant User as Пользователь
    participant MobileOperator as Сотовый оператор
    participant Netflix as Netflix
    participant Database as База данных

    User->>MobileOperator: Запрос списка доступных подписок (GET /subscriptions)
    MobileOperator->>Database: Запрос всех подписок
    Database-->>MobileOperator: Возвращает список подписок
    MobileOperator-->>User: Возвращает список подписок
    
    User->>MobileOperator: Запрос на подписку (POST /users/{userId}/subscriptions/{subscriptionId})
    MobileOperator->>Database: Создание записи подписки
    Database-->>MobileOperator: Подтверждение создания
    
    MobileOperator->>Netflix: Уведомление о подписке
    Netflix-->>MobileOperator: Подтверждение подписки
    
    MobileOperator-->>User: Подтверждение успешной подписки
    MobileOperator->>Database: Обновление данных о пользователе и подписке
```

## Часть два

### Изменения для разных вариантов списания

* Если добавить вариации списания (ежедневное или еженедельное), потребуется:

1) Изменения в процессе списания

+ Разработка логики для ежедневного и еженедельного списания средств
+ Обновление базы данных для хранения частоты списаний (то что добавилось поле period и в целом стоимость разных списаний)

2) Изменения на стороне оператора

+ Настройка новых расписаний списаний
+ Обновление интерфейсов для отображения новых опций по списанию

3) Изменения на стороне Netflix

+ Возможность приема данных о более частом списании (если такового не было)
+ Адаптация системы для учета частоты оплат (если такового не было)

## Часть три

Не имел опыта ни работы, ни пользования